# Desafio CloudWalk - Versão Script

##Instruções para rodar o desafio

### Versão da linguagem Ruby
Esse software foi desenvolvido em Ruby, na versão 3.0

### Obtendo o software
Clone esse repositório. Entre no diretório clonado:
```sh
$ cd quake_parser_script
```
E baixe a dependência usando o comando bundle:
```sh
$ bundle
```
Após essa etapa, já é possível rodar os testes, usando o comando abaixo:
```sh
$ rspec
```

##Utilizando o software
No diretório onde esse repositório foi clonado, rode o seguinte comando:
```sh
$ ruby bootstrap.rb /caminho/do/arquivo/log
```

Para usar o arquivo de exemplo, rode:
```sh
$ ruby bootstrap.rb spec/fixtures/qgames.log
```

O resultado será gerado em arquivos json no diretório output