class Ranking 
  attr_reader :ranking

  def initialize
    @ranking = {}
  end

  def update_ranking(match)
    match.score.each do |player, score|
      @ranking[player] ||= 0
      @ranking[player] += score
    end
  end
end