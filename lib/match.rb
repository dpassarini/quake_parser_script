require 'set'

class Match 
  attr_reader :players, :score, :total_kills, :kills_by_means

  def initialize
    @players = Set.new
    @score = {}
    @total_kills = 0
    @kills_by_means = {}
  end

  def add_player(player)
    @players.add(player)
  end

  def add_engaged_players(kill)
    add_player(kill.killer) if kill.killer != '<world>'
    add_player(kill.victim)
  end

  def increase_total_kills! 
    @total_kills += 1
  end

  def update_kills_by_means(kill)
    @kills_by_means[kill.cause] ||= 0
    @kills_by_means[kill.cause] += 1
  end

  def update_score(kill) 
    if kill.killer == '<world>'
      @score[kill.victim] ||= 0
      @score[kill.victim] -= 1
    else
      @score[kill.killer] ||= 0
      @score[kill.killer] += 1
    end
  end

  def update_match(kill)
    add_engaged_players(kill)
    update_score(kill)
    update_kills_by_means(kill)
    increase_total_kills!
  end

  def to_json
    {
      total_kills: @total_kills,
      players: @players.to_a,
      kills: @score
    }
  end
end