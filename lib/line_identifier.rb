require_relative 'match'
require_relative 'kill'

class LineIdentifier
  def data_from_line(line)
    return Match.new if line.include?('InitGame')

    return data_kill(line) if line.include?('Kill')
  end

  private
  def data_kill(kill_line)
    splitted_line = kill_line.split(' ')
    return Kill.new(
      killer: find_killer(splitted_line),
      victim: find_victim(splitted_line),
      cause: splitted_line.last
    )
  end

  def find_killer(kill_line)
    killer = []
    kill_line[5..].each do |line_item|
      break if line_item == 'killed'
      killer << line_item
    end

    killer.join(' ')
  end

  def find_victim(kill_line)
    victim = []
    kill_line[..-3].reverse_each do |line_item|
      break if line_item == 'killed'
      victim << line_item
    end

    victim.reverse.join(' ')
  end
end