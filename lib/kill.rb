class Kill
  attr_reader :killer, :victim, :cause

  def initialize(killer:, victim:, cause: )
    @killer = killer 
    @victim = victim
    @cause = cause
  end
end