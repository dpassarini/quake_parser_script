require 'spec_helper'
require_relative '../lib/match'
require_relative '../lib/kill'
require_relative '../lib/ranking'
require 'byebug'

describe Ranking do 
  describe '#update_ranking' do 
    it 'should add the match score to the ranking' do 
      match1 = Match.new
      match2 = Match.new

      kill1 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      kill2 = Kill.new(killer: 'Carlos', victim: 'Zé', cause: 'MOD_NOOB')
      kill3 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_HAND')
      kill4 = Kill.new(killer: 'Zé', victim: 'Carlos', cause: 'MOD_HAND')
      kill5 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')

      match1.update_match(kill1)
      match1.update_match(kill2)
      match1.update_match(kill3)
      match1.update_match(kill4)
      match1.update_match(kill5)

      match2.update_match(kill1)
      match2.update_match(kill2)
      match2.update_match(kill3)
      match2.update_match(kill4)
      match2.update_match(kill5)

      subject.update_ranking(match1)
      subject.update_ranking(match2)

      expect(subject.ranking).to eq({"Zé" => 4 , "Carlos" => 2 })
    end
  end
end