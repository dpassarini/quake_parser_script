require 'spec_helper'
require_relative '../lib/match'
require_relative '../lib/kill'

describe Match do
  describe '#add_player' do
    it 'should add player to a match' do 
      subject.add_player('Zé')
      subject.add_player('Osmar')
      
      expect(subject.players.to_a).to eq(['Zé', 'Osmar'])
    end

    it 'should not add the same player again' do 
      subject.add_player('Zé')
      subject.add_player('Osmar')
      subject.add_player('Raul')
      subject.add_player('Zé')
      
      expect(subject.players.to_a).to eq(['Zé', 'Osmar', 'Raul'])
    end
  end

  describe '#add_engaged_players' do
    it 'should add all players but <world>' do 
      kill1 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      kill2 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')
      kill3 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_HAND')
      kill4 = Kill.new(killer: 'Pedro', victim: 'Raul', cause: 'MOD_NOOB')

      subject.add_engaged_players(kill1)
      subject.add_engaged_players(kill2)
      subject.add_engaged_players(kill3)
      subject.add_engaged_players(kill4)

      expect(subject.players.to_a).to eq(['Zé', 'Haroldo', 'Pedro', 'Raul'])
    end
  end 

  describe '#update_score' do 
    it 'should add 3 kills to Zé' do 
      kill = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      subject.update_score(kill)
      subject.update_score(kill)

      expect(subject.score['Zé']).to eq(2)
    end

    it 'should penalize Zé by 1 kill, because of a world kill' do 
      kill = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      world_kill = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')
      subject.update_score(kill)
      subject.update_score(kill)
      subject.update_score(world_kill)

      expect(subject.score['Zé']).to eq(1)
    end
  end

  describe '#update_kills_by_means' do
    it 'should summarize the kills by the means' do
      kill1 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      kill2 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')
      kill3 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_HAND')
      kill4 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')

      subject.update_kills_by_means(kill1)
      subject.update_kills_by_means(kill2)
      subject.update_kills_by_means(kill3)
      subject.update_kills_by_means(kill4)

      expected_kills_by_means_response = {'MOD_GUN'=> 1, 'MOD_NOOB' => 2, 'MOD_HAND' => 1}

      expect(subject.kills_by_means).to eq(expected_kills_by_means_response)
    end
  end 

  describe '#update_match' do 
    it 'should update all match data' do 
      kill1 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      kill2 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')
      kill3 = Kill.new(killer: 'Zé', victim: 'Raul', cause: 'MOD_HAND')
      kill4 = Kill.new(killer: 'Haroldo', victim: 'Zé', cause: 'MOD_NOOB')

      subject.update_match(kill1)
      subject.update_match(kill2)
      subject.update_match(kill3)
      subject.update_match(kill4)

      expected_kills_by_means_response = {'MOD_GUN'=> 1, 'MOD_NOOB' => 2, 'MOD_HAND' => 1}

      expected_score = {'Zé'=> 1, 'Haroldo' => 1}

      expect(subject.kills_by_means).to eq(expected_kills_by_means_response)
      expect(subject.score).to eq(expected_score)
      expect(subject.total_kills).to eq(4)
      expect(subject.players.to_a).to eq(['Zé', 'Haroldo', 'Raul'])
    end
  end

  describe '#to_json' do
    it 'should format the data to the report' do
      kill1 = Kill.new(killer: 'Zé', victim: 'Haroldo', cause: 'MOD_GUN')
      kill2 = Kill.new(killer: '<world>', victim: 'Zé', cause: 'MOD_NOOB')
      kill3 = Kill.new(killer: 'Zé', victim: 'Raul', cause: 'MOD_HAND')
      kill4 = Kill.new(killer: 'Haroldo', victim: 'Zé', cause: 'MOD_NOOB')

      subject.update_match(kill1)
      subject.update_match(kill2)
      subject.update_match(kill3)
      subject.update_match(kill4)

      expected_response = {
        total_kills: 4,
        players: ['Zé', 'Haroldo', 'Raul'],
        kills: {'Zé'=> 1, 'Haroldo' => 1}
      }
    
      expect(subject.to_json).to eq(expected_response)
    end
  end

  describe '#increase_total_kills!' do
    it { expect { subject.increase_total_kills! }.to change(subject, :total_kills).by(1) }
  end
end