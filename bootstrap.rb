require_relative 'lib/ranking'
require_relative 'lib/line_identifier'
require 'json'

caminho_arquivo = ARGV[0]

lines = File.read(caminho_arquivo).split("\n")

line_identifier = LineIdentifier.new

total_matches = {}

first_match = true

matches_counter = 0

current_match = nil

kills_by_means = {}

ranking = Ranking.new

lines.each do |line|
  data = line_identifier.data_from_line(line)
  
  next if data.nil?
  
  if data.is_a?(Match)
    if !first_match
      ranking.update_ranking(current_match)
      total_matches["game_#{matches_counter}"] = current_match.to_json
      kills_by_means["game_#{matches_counter}"] = current_match.kills_by_means
    else
      first_match = false
    end
    
    current_match = data 
    matches_counter += 1 
  else # is a kill  
    current_match.update_match(data)
  end
end

# Update with the last match
ranking.update_ranking(current_match)
total_matches["game_#{matches_counter}"] = current_match.to_json
kills_by_means["game_#{matches_counter}"] = current_match.kills_by_means

File.open('output/kills.json', 'w'){|f| f.write(JSON.generate(total_matches))}
File.open('output/ranking.json', 'w'){|f| f.write(JSON.generate(ranking.ranking.sort_by{ |_, v| -v }))}
File.open('output/kills_by_means.json', 'w'){|f| f.write(JSON.generate(kills_by_means))}